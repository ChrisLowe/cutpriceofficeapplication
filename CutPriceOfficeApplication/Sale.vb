﻿Public Class Sale

    Private saleTimestamp As DateTime
    Private product As Product
    Private salePrice As Single
    Private quantityPurchased As Integer




    Public Function totalCost() As Single
        Dim q = Convert.ToSingle(quantityPurchased)
        Return (q * salePrice)
    End Function

    Public Sub New()

    End Sub

    Public Sub New(ByRef theProduct As Product, ByVal quantity As Single)
        salePrice = theProduct.cost * 1.6
        product = theProduct
        quantityPurchased = quantity
        saleTimestamp = DateValue(Now)
    End Sub


    Public Property saleTime() As DateTime
        Get
            Return saleTimestamp
        End Get
        Set(ByVal value As DateTime)
            saleTimestamp = value
        End Set
    End Property

    Public Property item() As Product
        Get
            Return product
        End Get
        Set(ByVal value As Product)
            product = value  'should never need this
        End Set
    End Property


    Public Property price() As Single
        Get
            Return salePrice
        End Get
        Set(ByVal value As Single)
            salePrice = value
        End Set
    End Property

    Public Property quantity As Integer
        Get
            Return quantityPurchased
        End Get
        Set(ByVal value As Integer)
            quantityPurchased = value
        End Set
    End Property


End Class
