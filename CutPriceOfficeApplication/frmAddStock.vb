﻿Public Class frmAddStock

    Dim myStore As Store
    Dim myProduct As Product


    Public Sub setStore(ByRef theStore As Store, ByRef theProduct As Product)
        myStore = theStore
        myProduct = theProduct
        lblProductName.Text = myProduct.name
        lblCurrentStock.Text = myProduct.numberInStock
    End Sub


    Public Sub confirmUpdate()
        Dim inputValue = DecimalnputControl1.inputInteger
        myStore.addInventory(myProduct, inputValue)
        DecimalnputControl1.clear()
        frmSale.txtProductStock.Text = myProduct.numberInStock
        Me.Visible = False
    End Sub

    Public Sub enterPressed() Handles DecimalnputControl1.enterPressed
        confirmUpdate()
    End Sub



    Private Sub btnConfirm_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConfirm.Click
        confirmUpdate()
    End Sub

    Private Sub frmAddStock_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        DecimalnputControl1.clear()
        Me.CenterToScreen()

    End Sub

    Private Sub DecimalnputControl1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DecimalnputControl1.Load

    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Visible = False
    End Sub
End Class