﻿Imports System.IO


Public Class Store

    Public items As New List(Of Product)
    Public sales As New List(Of Sale)


    Public Sub addInventory(ByRef product As Product, ByVal quantity As Integer)
        Dim idx = items.IndexOf(product)
        items.Item(idx).numberInStock += quantity
        Dim message As String
        message = items.Item(idx).name & " stock is now " & items.Item(idx).numberInStock
        MessageBox.Show(message)
    End Sub

    Public Sub addSale(ByRef product As Product, ByVal quantity As Integer)
        Dim s As New Sale(product, quantity)
        sales.Add(s)
    End Sub

    Public Sub makeSale()

        Dim s As Sale
        Dim p As Product
        Dim message As String
        message = "Sold: " & vbNewLine

        For Each s In sales
            For Each p In items
                If s.item.code = p.code Then
                    'Deduct the quantity purchased from the number in stock
                    p.numberInStock -= s.quantity

                    ' Construct a message
                    message = message & s.quantity & " x " & p.name
                    If (s.quantity > 1) Then
                        message = message & "'s" & vbNewLine
                    Else
                        message = message & vbNewLine
                    End If


                End If
            Next
        Next

        MessageBox.Show(message)

    End Sub

    Private Function createSalesString(ByRef s As Sale) As String
        Dim message As String
        message = s.saleTime & "," & s.item.code & "," & s.quantity & "," & s.totalCost()
        Return message
    End Function






    Public Sub saveSalesFile()

        Dim outFile As StreamWriter
        Dim filename = Application.StartupPath & "\sales.txt"
        Dim message As String
        message = ""

        ' Create the file if it does not exist
        'If Not (File.Exists(filename)) Then
        'outFile = File.CreateText(filename)
        'End If

        ' Only append to the sales file
        outFile = File.AppendText(filename)

        ' Write a line representing the sales
        Dim s As Sale
        For Each s In sales
            message = message & createSalesString(s) & vbNewLine
        Next

        outFile.Write(message)

        outFile.Close()

    End Sub


    Private Function createWriteString(ByRef p As Product)
        Dim str = p.code & "," & p.name & "," & p.numberInStock & "," & p.cost & "," & p.imageFile
        Return str
    End Function

    Public Sub saveProductFile()

        Dim outFile As StreamWriter
        Dim filename = Application.StartupPath & "\products.txt"

        ' Post an error if the products file is missing
        If Not (File.Exists(filename)) Then
            MessageBox.Show("The file " + filename + " is missing.")
            Exit Sub
        End If

        outFile = File.CreateText(filename)

        ' Write the product to a text file
        Dim p As Product
        For Each p In items
            Dim writeString = createWriteString(p)
            outFile.WriteLine(writeString)
        Next

        outFile.Close()

    End Sub



    Public Function salesHistory() As List(Of Sale)
        Dim recordedSales As New List(Of Sale)

        Dim inFile As StreamReader
        Dim filename = Application.StartupPath & "\sales.txt"
        Dim readString As String
        Dim fields(4) As String

        ' Validate the filename
        If Not (File.Exists(filename)) Then
            MessageBox.Show("The file " + filename + " is missing.")
        End If

        inFile = File.OpenText(filename)
        readString = inFile.ReadLine()

        ' Read each line in the sales file
        Do While (readString <> "")
            fields = readString.Split(",")

            Dim s As New Sale()
            Dim p As Product
            Dim match As Product
            match = items.Item(0)  ' this may cause problems .. should not be here but prevents a syntax error

            's.saleTime = Convert.ToDateTime(fields(0)
            DateTime.TryParse(fields(0), s.saleTime)



            ' Find a match to the product
            Dim pCode = fields(1)
            For Each p In items
                If p.code = pCode Then
                    match = p
                End If
            Next

            s.item = match
            s.quantity = fields(2)

            ' Add the recorded sales to the interim collection
            recordedSales.Add(s)

            ' Next record
            readString = inFile.ReadLine()
        Loop

        Return recordedSales

    End Function


    Public Sub loadProductFile()

        Dim inFile As StreamReader
        Dim readString As String
        Dim fields(4) As String

        Dim filename = Application.StartupPath & "\products.txt"

        ' Empty the collection
        items.Clear()

        ' Validate the filename (static, should be a once only check)
        If Not (File.Exists(filename)) Then
            MessageBox.Show("The file " + filename + " is missing.")
            Return
        End If

        ' Read the file line by line and populate the items collection with the data
        inFile = File.OpenText(filename)
        readString = inFile.ReadLine()
        Do While (readString <> "")

            ' Process record
            fields = readString.Split(",")
            Dim pCode = fields(0)
            Dim pName = fields(1)
            Dim pNumberInStock = Convert.ToInt32(fields(2))
            Dim pCost = Convert.ToSingle(fields(3))
            Dim pImageFile = fields(4)
            Dim p As New Product(pCode, pName, pCost, pNumberInStock, pImageFile)

            ' Add record to collection
            items.Add(p)

            ' Next record
            readString = inFile.ReadLine()
        Loop

        inFile.Close()

    End Sub





End Class
