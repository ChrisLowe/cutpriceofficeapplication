﻿Public Class frmSalesHistory

    Dim totalSales As Double

    Private Function toSalesString(ByRef mySale As Sale)
        Dim str As String
        Dim cost As Double
        cost = Convert.ToDouble(mySale.quantity * mySale.item.salePrice)
        str = "On " & mySale.saleTime & " sold: " & mySale.quantity & " x " & mySale.item.code & " = " & cost.ToString("C")
        totalSales += cost
        Return str
    End Function


    Public Sub loadSalesHistory(ByRef myStore As Store)

        Dim salesHistory As List(Of Sale)
        salesHistory = myStore.salesHistory()

        Dim s As Sale
        For Each s In salesHistory
            lbxSalesHistory.Items.Add(toSalesString(s))
            totalSales += s.totalCost
        Next

        lblTotalSales.Text = totalSales.ToString("c")

    End Sub



    Private Sub frmSalesHistory_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.CenterToScreen()
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.Visible = False
    End Sub
End Class