﻿Public Class frmConfirm

    Dim amountText As String

    Public Sub setAmountText(ByVal value As Double)
        lblAmount.Text = value.ToString("C")


    End Sub


    Private Sub frmConfirm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        amountText = "$00.00"

        CenterToScreen()
    End Sub

    Private Sub btnConfirm_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConfirm.Click
        frmSale.saleConfimed()
        Me.Visible = False
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Visible = False
    End Sub
End Class