﻿
Public Class Product


    Private productCode As String
    Private productName As String
    Private productCost As Single
    Private productImageFile As String
    Private productNumberInStock As Integer
    Private productSalePrice As Single


    Public Sub New(ByVal code As String, ByVal name As String, ByVal cost As Single, ByVal numberInStock As Integer, ByVal imageFile As String)
        productCode = code
        productName = name
        productCost = cost
        productNumberInStock = numberInStock
        productImageFile = imageFile
    End Sub

    Public Property salePrice() As String
        Set(ByVal value As String)
            productCost = 1.6
        End Set
        Get
            Return (productCost * 1.6)
        End Get
    End Property

    Public Property code() As String
        Get
            Return productCode
        End Get
        Set(ByVal value As String)
            productCode = value
        End Set
    End Property


    Public Function stockAvailable() As Boolean
        If (productNumberInStock > 0) Then
            Return True
        Else
            Return False
        End If
    End Function


    Public Sub makeSale()
        If (productNumberInStock > 0) Then
            productNumberInStock -= 1
        End If
    End Sub


    Public Property numberInStock() As Integer
        Get
            Return productNumberInStock
        End Get
        Set(ByVal value As Integer)
            productNumberInStock = value
        End Set
    End Property


    Public Property name() As String
        Get
            Return productName
        End Get
        Set(ByVal value As String)
            productName = value
        End Set
    End Property


    Public Property cost() As Single
        Get
            Return productCost
        End Get
        Set(ByVal value As Single)
            productCost = value
        End Set
    End Property


    Public Property imageFile() As String
        Get
            Return productImageFile
        End Get
        Set(ByVal value As String)
            productImageFile = value
        End Set
    End Property

End Class
