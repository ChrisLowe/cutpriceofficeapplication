﻿Public Class frmQuantity


    Private Sub confirmInput()
        Dim inputValue = DecimalnputControl1.inputInteger
        DecimalnputControl1.clear()
        frmSale.makeSale(inputValue)
        Me.Visible = False
    End Sub

    Private Sub btnConfirm_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConfirm.Click
        confirmInput()
    End Sub


    Private Sub enterPressed() Handles DecimalnputControl1.enterPressed
        confirmInput()
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        DecimalnputControl1.clear()
        Me.Visible = False

    End Sub

    Private Sub frmQuantity_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.CenterToScreen()
        DecimalnputControl1.txtQuantity.Focus()
    End Sub
End Class