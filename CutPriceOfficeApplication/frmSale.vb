﻿Public Class frmSale

    Public myStore As New Store
    Dim activeProduct As Product
    Dim subTotal As Double



    ' Used by frmQuantity to set the quantity
    Public Sub makeSale(ByVal quantity As Integer)
        Dim available = activeProduct.numberInStock

        If (quantity > available) Then
            MessageBox.Show("There is insufficent " & activeProduct.name & "'s in stock. (" & available & " available)")
        Else
            ' Add it to the basket and update the subtotal
            lbxBasket.Items.Add(activeProduct.name & " x " & quantity)
            lbxCost.Items.Add((quantity * activeProduct.salePrice).ToString("C"))
            subTotal += Convert.ToDouble(quantity * activeProduct.salePrice)
            txtTotal.Text = subTotal.ToString("C")

            ' Register the sale
            myStore.addSale(activeProduct, quantity)

            ' Update the current available stock (but do not save it yet)
            Dim idx = myStore.items.IndexOf(activeProduct)
            myStore.items(idx).numberInStock -= quantity
            loadProduct(idx)
            'txtProductStock.Text = Convert.ToInt32(txtProductStock.Text) - quantity
        End If
    End Sub

    ' Used by frmConfirm to confirm the sale
    Public Sub saleConfimed()
        myStore.makeSale()
        clearSale()
    End Sub



    Private Sub frmSale_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        CenterToScreen()

        btnSales.Enabled = True
        btnAddStock.Enabled = True
        btnEdit.Enabled = False
        btnDelete.Enabled = False
        btnNew.Enabled = False

        Dim count = 0
        subTotal = 0

        myStore.loadProductFile()

        Dim p As Product
        For Each p In myStore.items
            lbxItems.Items.Add(p.name)
        Next

        'While (count < myStore.items.Count)
        '    Dim p As Product
        '    p = myStore.items.Item(count)
        '    lbxItems.Items.Add(p.name)
        '    count += 1
        'End While

        loadProduct(0)


    End Sub

    Private Sub loadProduct(ByVal index As Integer)
        Dim p As Product
        p = myStore.items.Item(index)
        activeProduct = p

        txtProductName.Text = p.name
        txtProductCode.Text = p.code
        txtSalePrice.Text = Convert.ToDouble(p.salePrice).ToString("C")
        txtProductStock.Text = p.numberInStock

        PictureBox.Image = Image.FromFile(p.imageFile)
    End Sub

    Private Sub lbxItems_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbxItems.SelectedIndexChanged
        Dim idx = lbxItems.SelectedIndex
        loadProduct(idx)
    End Sub

    Private Sub bntSale_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bntSale.Click
        frmQuantity.Show()
    End Sub

    Public Sub clearSale()

        'Need to restore any changes made to the items file
        Dim s As Sale
        For Each s In myStore.sales
            Dim idx = myStore.items.IndexOf(s.item)
            myStore.items(idx).numberInStock += s.quantity
        Next

        lbxBasket.Items.Clear()
        lbxCost.Items.Clear()
        myStore.sales.Clear()
        subTotal = 0
        Dim i = myStore.items.IndexOf(activeProduct)
        txtProductStock.Text = myStore.items.Item(i).numberInStock
        txtTotal.Text = ""
        loadProduct(0)

    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        clearSale()
    End Sub

    Private Sub btnConfirmSale_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConfirmSale.Click
        frmConfirm.Visible = True
        frmConfirm.setAmountText(subTotal)
        myStore.saveProductFile()
        myStore.saveSalesFile()
    End Sub




    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.Close()
    End Sub

    Private Sub btnAddStock_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddStock.Click
        Dim idx = lbxItems.SelectedIndex()

        frmAddStock.Visible = True
        frmAddStock.setStore(myStore, activeProduct)
    End Sub


    Private Sub btnSales_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSales.Click
        frmSalesHistory.loadSalesHistory(myStore)
        frmSalesHistory.Visible = True
    End Sub

End Class
