﻿Public Class DecimalnputControl

    Dim inputInt As Integer
    Dim firstInput = True

    Event enterPressed()

    Public Property inputInteger() As Integer
        Get
            Return Convert.ToInt32(txtQuantity.Text)
        End Get
        Set(ByVal value As Integer)

        End Set
    End Property

    Public Sub clear()
        txtQuantity.Text = "1"
        firstInput = True
    End Sub


    Private Sub setQuantity(ByVal theInput As Integer)
        If (firstInput = True) Then
            If (theInput > 0) Then
                txtQuantity.Text = theInput
                firstInput = False
            End If

        Else
            txtQuantity.Text = txtQuantity.Text & theInput
        End If
        txtQuantity.Focus()
    End Sub


    Private Sub btn1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn1.Click
        setQuantity(1)
    End Sub

    Private Sub btn2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn2.Click
        setQuantity(2)
    End Sub

    Private Sub btn3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn3.Click
        setQuantity(3)
    End Sub

    Private Sub btn4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn4.Click
        setQuantity(4)
    End Sub

    Private Sub btn5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn5.Click
        setQuantity(5)
    End Sub

    Private Sub btn6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn6.Click
        setQuantity(6)
    End Sub

    Private Sub btn7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn7.Click
        setQuantity(7)
    End Sub

    Private Sub btn8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn8.Click
        setQuantity(8)
    End Sub

    Private Sub btn9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn9.Click
        setQuantity(9)
    End Sub


    Private Sub btn0_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn0.Click
        setQuantity(0)

    End Sub

    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        txtQuantity.Text = "0"
        firstInput = True
        txtQuantity.Focus()
    End Sub

    Private Sub UserControl1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs)
        txtQuantity.Text = "1"
        txtQuantity.Focus()
    End Sub

    Private Sub DecimalnputControl_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txtQuantity.Focus()

    End Sub

    Private Sub txtQuantity_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtQuantity.KeyDown
        If e.KeyValue = Keys.Enter Then
            RaiseEvent enterPressed()
        End If
    End Sub



    Private Sub txtQuantity_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtQuantity.TextChanged
        Try
            Convert.ToInt32(txtQuantity.Text)
        Catch ex As Exception

            ' Allow the user to delete the '1'
            If Not (txtQuantity.Text = "") Then
                MessageBox.Show("Invalid data. Only numbers are valid.")
                txtQuantity.Text = "1"
                txtQuantity.Focus()
            End If
        End Try
    End Sub
End Class
